use super::prelude::*;

#[derive(Template)]
#[template(path = "suggest.html")]
pub struct Suggest {
    pub page_title: String,

    pub css_main: &'static str,
    pub css_icons: &'static str,
    pub css_exp: &'static str,
}

impl Suggest {
    pub fn responder(_req: &HttpRequest<Database>) -> impl Responder {
        Suggest::default()
    }
}

impl Default for Suggest {
    fn default() -> Suggest {
        Suggest {
            page_title: "Suggest".to_string(),

            css_main: CSS_MAIN,
            css_icons: CSS_ICONS,
            css_exp: CSS_EXP,
        }
    }
}
