use super::prelude::*;

#[derive(Template)]
#[template(path = "faq.html")]
pub struct Faq {
    pub page_title: String,

    pub css_main: &'static str,
    pub css_icons: &'static str,
    pub css_exp: &'static str,
}

impl Faq {
    pub fn responder(_req: &HttpRequest<Database>) -> impl Responder {
        Faq::default()
    }
}

impl Default for Faq {
    fn default() -> Faq {
        Faq {
            page_title: "FAQ".to_string(),

            css_main: CSS_MAIN,
            css_icons: CSS_ICONS,
            css_exp: CSS_EXP,
        }
    }
}
