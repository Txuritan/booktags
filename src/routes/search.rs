use super::prelude::*;

#[derive(Template)]
#[template(path = "search.html")]
pub struct Search {
    pub page_title: String,

    pub css_main: &'static str,
    pub css_icons: &'static str,
    pub css_exp: &'static str,
}

#[derive(Debug, Deserialize)]
pub struct SearchForm {
    pub search: String,
}

impl Search {
    pub fn responder((_state, form): (State<Database>, Option<Form<SearchForm>>)) -> impl Responder {
        info!("Search: {}", form.is_some());

        if let Some(f) = form {
            info!("Search: {}", f.search);
        }

        Search::default()
    }
}

impl Default for Search {
    fn default() -> Search {
        Search {
            page_title: "Search".to_string(),

            css_main: CSS_MAIN,
            css_icons: CSS_ICONS,
            css_exp: CSS_EXP,
        }
    }
}
