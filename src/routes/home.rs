use super::prelude::*;

#[derive(Template)]
#[template(path = "home.html")]
pub struct Home {
    pub page_title: String,

    pub css_main: &'static str,
    pub css_icons: &'static str,
    pub css_exp: &'static str,

    pub version: &'static str,
}

impl Home {
    pub fn responder(_req: &HttpRequest<Database>) -> impl Responder {
        Home::default()
    }
}

impl Default for Home {
    fn default() -> Home {
        Home {
            page_title: "Home".to_string(),

            css_main: CSS_MAIN,
            css_icons: CSS_ICONS,
            css_exp: CSS_EXP,

            version: env!("CARGO_PKG_VERSION"),
        }
    }
}
