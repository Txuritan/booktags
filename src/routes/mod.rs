mod admin;
mod book;
mod faq;
mod home;
mod search;
mod suggest;

pub const CSS_MAIN: &'static str = include_str!("../../assets/spectre.min.css");
pub const CSS_EXP: &'static str = include_str!("../../assets/spectre-exp.min.css");
pub const CSS_ICONS: &'static str = include_str!("../../assets/spectre-icons.min.css");

pub use self::faq::Faq;
pub use self::home::Home;
pub use self::search::Search;
pub use self::suggest::Suggest;

pub mod prelude {
    pub use actix_web::{Form, HttpRequest, Responder, State};

    pub use askama::Template;

    pub use crate::database::Database;

    pub use super::{
        CSS_MAIN, CSS_EXP, CSS_ICONS,
    };
}