#[macro_use]
extern crate log;
#[macro_use(quick_error)]
extern crate quick_error;
#[macro_use]
extern crate serde_derive;

mod database;
mod error;

mod routes;

use std::env;

use actix_web::{actix::*, http, middleware, server, App, HttpResponse};

use env_logger::Target;

use crate::{database::Database, error::Error, routes::*};

pub const IMAGE_POST: &[u8] = include_bytes!("../assets/katy-l-woods-post.jpg");

fn main() -> Result<(), Error> {
    pretty_env_logger::formatted_builder()
        .target(Target::Stdout)
        .parse("info")
        .init();

    let port = env::var("PORT")
        .unwrap_or_else(|_| "3000".to_string())
        .parse()?;

    let db = Database::load()?;
    let _ = db.setup()?;

    let sys = System::new("archive");

    server::new(move || {
        App::with_state(db.clone())
            .middleware(middleware::Logger::default())
            .resource("/", |r| r.f(Home::responder))
            .resource("/faq", |r| r.f(Faq::responder))
            .resource("/search", |r| r.method(http::Method::GET).with(Search::responder))
            .resource("/suggest", |r| r.f(Suggest::responder))
            .resource("/katy-l-woods-post.jpg", |r| {
                r.f(|_| {
                    HttpResponse::Ok()
                        .content_type("image/jpeg")
                        .body(IMAGE_POST)
                })
            })
    })
    .bind(("0.0.0.0", port))?
    .start();

    let _ = sys.run();

    Ok(())
}
