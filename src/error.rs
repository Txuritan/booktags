use std::env::VarError;
use std::io::Error as IoError;
use std::num::ParseIntError;

use postgres::Error as PostgresError;
use r2d2::Error as R2D2Error;

quick_error! {
    #[derive(Debug)]
    pub enum Error {
        Var(err: VarError) {
            from()
            description("Var error")
            display("Var error: {}", err)
            cause(err)
        }
        Io(err: IoError) {
            from()
            description("Io error")
            display("Io error: {}", err)
            cause(err)
        }
        ParseInt(err: ParseIntError) {
            from()
            description("ParseInt error")
            display("ParseInt error: {}", err)
            cause(err)
        }

        R2D2(err: R2D2Error) {
            from()
            description("R2D2 error")
            display("R2D2 error: {}", err)
            cause(err)
        }
        Postgres(err: PostgresError) {
            from()
            description("Postgres error")
            display("Postgres error: {}", err)
            cause(err)
        }
    }
}
