use std::env;

use actix_web::actix::*;

use r2d2::{Pool, PooledConnection};
use r2d2_postgres::{PostgresConnectionManager, TlsMode};

use crate::error::Error;

const SQL: &[(&str, &str)] = &[
    ("authors", include_str!("../sql/create/authors.sql")),
    ("books", include_str!("../sql/create/books.sql")),
    ("languages", include_str!("../sql/create/languages.sql")),
    ("series", include_str!("../sql/create/series.sql")),
    ("tags", include_str!("../sql/create/tags.sql")),
    ("author-books", include_str!("../sql/create/author-books.sql")),
    ("book-language", include_str!("../sql/create/book-language.sql")),
    ("book-series", include_str!("../sql/create/book-series.sql")),
    ("book-tags", include_str!("../sql/create/book-tags.sql")),
];

pub type PostgresConn = PooledConnection<PostgresConnectionManager>;
pub type PostgresPool = Pool<PostgresConnectionManager>;

pub struct Database {
    pub pool: PostgresPool,
}

impl Database {
    pub fn load() -> Result<Database, Error> {
        let url = env::var("DATABASE_URL")
            .unwrap_or_else(|_| "postgres://booktags:booktags@127.0.0.1:5432/booktags".to_string());

        info!("Connecting to database");

        let manager = PostgresConnectionManager::new(url, TlsMode::None)?;

        info!("Connected to database");

        let pool = Pool::new(manager)?;

        Ok(Database { pool })
    }

    pub fn setup(&self) -> Result<(), Error> {
        let conn = &self.get()?;

        conn.execute("CREATE EXTENSION IF NOT EXISTS isn;", &[])?;

        for sql in SQL.iter() {
            trace!("Executing SQL: {}", sql.0);

            conn.execute(sql.1, &[])?;
        }

        Ok(())
    }

    pub fn get(&self) -> Result<PostgresConn, Error> {
        let conn = self.pool.clone().get()?;

        Ok(conn)
    }
}

impl Actor for Database {
    type Context = SyncContext<Self>;
}

impl Clone for Database {
    fn clone(&self) -> Database {
        Database {
            pool: self.pool.clone(),
        }
    }
}

pub struct Nothing;

impl Message for Nothing {
    type Result = Result<(), Error>;
}

impl Handler<Nothing> for Database {
    type Result = Result<(), Error>;

    fn handle(&mut self, _msg: Nothing, _: &mut Self::Context) -> Self::Result {
        Ok(())
    }
}
