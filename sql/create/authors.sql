CREATE TABLE
IF NOT EXISTS
    authors (
        id              VARCHAR(10)     NOT NULL    PRIMARY KEY,
        name            TEXT            NOT NULL,
        created         DATE            NOT NULL    DEFAULT CURRENT_DATE,
        updated         DATE            NOT NULL    DEFAULT CURRENT_DATE
    );
