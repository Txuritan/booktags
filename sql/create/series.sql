CREATE TABLE
IF NOT EXISTS
    series (
        id              VARCHAR(10)     NOT NULL    PRIMARY KEY,
        title           TEXT            NOT NULL,
        created         DATE            NOT NULL    DEFAULT CURRENT_DATE,
        updated         DATE            NOT NULL    DEFAULT CURRENT_DATE
    );