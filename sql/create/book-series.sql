CREATE TABLE
IF NOT EXISTS
    book_series (
        book_id         VARCHAR(10)     NOT NULL    REFERENCES books (id),
        series_id       VARCHAR(10)     NOT NULL    REFERENCES series (id),
        position        INTEGER         NOT NULL
    );