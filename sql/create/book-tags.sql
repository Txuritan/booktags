CREATE TABLE
IF NOT EXISTS
    book_tags (
        book_id         VARCHAR(10)     NOT NULL    REFERENCES books (id),
        tag_id          VARCHAR(10)     NOT NULL    REFERENCES tags (id)
    );
