CREATE TABLE
IF NOT EXISTS
    book_language (
        book_id         VARCHAR(10)     NOT NULL    REFERENCES books (id),
        languages_id    VARCHAR(10)     NOT NULL    REFERENCES languages (id)
    );