CREATE TABLE
IF NOT EXISTS
    books (
        id              VARCHAR(10)     NOT NULL    PRIMARY KEY,
        title           TEXT            NOT NULL,
        summary         TEXT            NOT NULL,
        release         DATE            NOT NULL,
        isbn10          ISBN            NOT NULL,
        isbn13          ISBN13          NOT NULL,
        created         DATE            NOT NULL    DEFAULT CURRENT_DATE,
        updated         DATE            NOT NULL    DEFAULT CURRENT_DATE
    );
