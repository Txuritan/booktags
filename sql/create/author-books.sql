CREATE TABLE
IF NOT EXISTS
    author_book (
        author_id       VARCHAR(10)     NOT NULL    REFERENCES authors (id),
        book_id         VARCHAR(10)     NOT NULL    REFERENCES books (id)
    );
